import '../style/todoItem.css';
import { useDispatch } from 'react-redux';
import { updateTodoList } from './todoSlice';
import { deleteTodoList } from './todoSlice';

export const TodoItem = (props) => {
     const dispatch = useDispatch();

     const handleClick = () => {
        dispatch(updateTodoList(props.index));
     }

     const handleDeleteClick = () => {
        dispatch(deleteTodoList(props.index));
     }

    return (
        <div className='todo-item'>
            <span className={props.value.done ? 'finish' : 'unfinish'} onClick={handleClick}>{props.value.text}</span>
            <button className='button' onClick={handleDeleteClick}>x</button>
        </div>
    )
}