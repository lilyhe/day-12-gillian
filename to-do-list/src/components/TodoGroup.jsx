import { TodoItem } from "./ToDoItem" 
export const TodoGroup = (props) => {
    return (
        <div>
            {props.todoList.map((value, index) => (<TodoItem key={index} value={value} index={index}></TodoItem>))}
        </div>
    )
}
