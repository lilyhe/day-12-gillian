import { createSlice } from '@reduxjs/toolkit';

const todoSlice = createSlice({
    name: "todo",
    initialState: {
        todoList: [],
    },
    reducers: {
        addTodoList: (state, action) => {
            state.todoList = [...state.todoList, action.payload];
        },
        updateTodoList: (state, action) => {
            state.todoList[action.payload].done = true;
        },
        deleteTodoList: (state, action) => {
            state.todoList.splice(action.payload, 1);
        },
    },

})

export const { addTodoList } = todoSlice.actions;
export const { updateTodoList } = todoSlice.actions;
export const { deleteTodoList } = todoSlice.actions;
export default todoSlice.reducer;