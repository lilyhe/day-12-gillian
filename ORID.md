# day12-Gillian

## Objective:
During code review and review on yesterday's practice, I figured out the working details of props with the explanation of classmates and teachers.
I learned Redux today. Redux is used to manage and update global state, which makes it easy to transfer data between components.
Redux has action and reducer, where the reducer must be a pure function.

## Reflective:
Difficult and very practical

## Interpretive:
Redux simplifies the passing of parameters, so today's exercise is easier to understand than yesterday and I can applied Redux quickly. But I'm not familiar with css, so I'm having trouble implementing styles.

## Decisional:
Review the exercises in class, do more exercises about css.










